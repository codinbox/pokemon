import { Component, Input, IterableDiffers, DoCheck, SimpleChanges } from '@angular/core';
import { Card } from './../card/card';
import { DeckService } from './deck.service';
import { Deck } from './Deck';
import { LoginService } from '../login/login.service';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-deck',
  templateUrl: './deck.component.html',
  styleUrls: ['./deck.component.css']
})
export class DeckComponent implements OnInit {
  deck: Deck;
  cards: Card[];
  player: string;
  iterableDiffer: any;

  constructor( 
    private _iterableDiffers: IterableDiffers,
    private _deckService: DeckService,
    private _loginService: LoginService
  ) {
    this.iterableDiffer = this._iterableDiffers.find([]).create(null);
  }

  ngOnInit(): void {
    this._loginService.player.subscribe(player => {
      this.player = player;
    });
    this._deckService.deck.subscribe(deck => {
      this.deck = deck;
      this.cards = deck.cards;
    });
  }

  saveDeck() {
    this._deckService.saveDeck(this.player);
  }

  loadDeck() {
    this._deckService.loadDeck(this.player);
  }
}
