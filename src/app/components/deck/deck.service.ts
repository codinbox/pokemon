import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Deck } from './Deck';
import { LoginService } from '../login/login.service';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { Card } from '../card/card';

@Injectable()
export class DeckService {
  private _deck = new BehaviorSubject<Deck>(<Deck>{cards: []});
  deck = this._deck.asObservable();
  private url: string = `https://codinbox-pokemon.herokuapp.com/deck/`;

  constructor(
    private _http: HttpClient,
    private _loginService: LoginService
  ) { }

  saveDeck(player: string)  {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    this._http.put(this.url + player, JSON.stringify(this._deck.getValue()), {headers: headers})
    .subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log("Error occured");
      }
    );
  }

  loadDeck(player): void {
    var deck = <Deck>{cards: []};
    this._http.get(this.url + player)
      .subscribe(data => {
        if(data[0]) {
          deck.cards = data[0]['cards'].map(function(element){
            return <Card>{
              id: element['id'],
              name: element['name'],
              image: element['image'],
              image_back: element['image_back'],
              type: element['type'],
              hp: element['hp'],
              speed: element['speed'],
              attack: element['attack'],
              defense: element['defense']
            };
          });
        }
        this._deck.next(deck);
      },
      error => {
        console.log('error: ' + error);
      });
  }

  addCard(card: Card): void {
    var deck = this._deck.getValue();
    deck.cards.push(card);
    this._deck.next(deck);
  }
}
