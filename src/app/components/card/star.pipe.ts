import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'star'
})
export class StarPipe implements PipeTransform {

  transform(value: number, args?: any): any {
    if(value>= 75) {
      return value + ' <i class="fas fa-star"></i>';
    }
    return value;
  }

}
