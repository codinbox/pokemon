export class Card {
    id : number;
    image : string;
    image_back : string;
    name : string;
    type : string;
    hp : number;
    speed: number;
    attack : number;
    defense : number;
}