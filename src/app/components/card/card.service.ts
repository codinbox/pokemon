import { Injectable } from '@angular/core';
import { Card } from './card';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs/observable/of';

@Injectable()
export class CardService { 
  //private url: string = 'https://pokeapi.co/api/v2/pokemon/';
  private url: string = 'https://jqa-course.kmt.orange.com/pokemon/';
  cards: Card[] = [];
  constructor(private _http: HttpClient) {}

  getRandomCards(qty:number = 1): Observable<Card[]> {
    this.cards = [];
    for(var i=0; i<qty; i++){
      var random = Math.floor(Math.random()*117 +1);
      this._http.get(this.url + random)
      .subscribe(data => {
        var card = <Card>{
          id : data['id'],
          name : data['forms'][0]['name'],
          image : data['sprites']['front_default'],
          image_back : data['sprites']['back_default']
        };
        
        var stats = data['stats'];
        stats.forEach(element => {
          card[element['stat']['name']] = element['base_stat'];
        });
        this.cards.push(card);
      })
    }
    return of(this.cards);
  }

  getCardById(id:number): Observable<Card> {
    var card;
    this._http.get(this.url + id)
    .subscribe(data => {
      card = <Card>{
        id : data['id'],
        name : data['forms'][0]['name'],
        image : data['sprites']['front_default'],
        image_back : data['sprites']['back_default']
      };
      
      var stats = data['stats'];
      stats.forEach(element => {
        card[element['stat']['name']] = element['base_stat'];
      });
    })
    return of(card);
  }
}
