import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class LoginService {
  private _player = new BehaviorSubject<string>("");
  player = this._player.asObservable();

  constructor() { }

  login(player): void {
    this._player.next(player);
  }
}
