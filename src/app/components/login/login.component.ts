import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { DeckService } from '../deck/deck.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  player: string;
  constructor(
    private _loginService: LoginService,
    private _deckService: DeckService) { }

  login() {
    console.log(this.player);
    this._loginService.login(this.player);
    this._deckService.loadDeck(this.player);
  }

}
