import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Card } from './../card/card';
import { CardService } from '../card/card.service';
import { DeckService } from '../deck/deck.service';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.css']
})
export class PackageComponent {
  cards: Card[] = [];
  filter: number;
  constructor(
    private _cardService: CardService,
    private _deckService: DeckService
  ) { }

  selectCard(card): boolean{
    this._deckService.addCard(card);
    this.cards.splice(this.cards.indexOf(card, 0), 1);
    return false;
  }

  newSelection(): void{
    this.getCards();
  }
  
  getCards(): void {
    this._cardService.getRandomCards(10)
        .subscribe(cards => {
          this.cards = cards;
        });
  }
}
