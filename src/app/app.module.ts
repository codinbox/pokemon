import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { EnteteComponent } from './components/entete/entete.component';
import { DeckComponent } from './components/deck/deck.component';
import { PackageComponent } from './components/package/package.component';
import { CardComponent } from './components/card/card.component';
import { LoginComponent } from './components/login/login.component';
import { CardService } from './components/card/card.service';
import { StarPipe } from './components/card/star.pipe';
import { DeckService } from './components/deck/deck.service';


@NgModule({
  declarations: [
    AppComponent,
    EnteteComponent,
    DeckComponent,
    PackageComponent,
    CardComponent,
    LoginComponent,
    StarPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [CardService, DeckService],
  bootstrap: [AppComponent]
})
export class AppModule { }
