import { Component } from '@angular/core';
import { Card } from './components/card/card';
import { CardService } from './components/card/card.service';
import { DeckService } from './components/deck/deck.service';
import { Deck } from './components/deck/Deck';
import { LoginService } from './components/login/login.service';

@Component({
  providers: [CardService, DeckService, LoginService],
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  player: string;
  title = 'app';
  constructor(
    private _cardService: CardService,
    private _deckService: DeckService,
    private _loginService: LoginService
  ){}
}
